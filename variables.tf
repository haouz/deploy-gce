variable "application_name" {
	type = string
	default = "demo-gce-jhz"
}
variable "machine_type" {
	type = string
	default = "n1-standard-1"
}
variable "gcp_project" {
	type = string
	default = "sfeir-school-terraform"
}
